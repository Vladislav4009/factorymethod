package by.shag.gritskevich.conf;

import by.shag.gritskevich.model.Book;
import by.shag.gritskevich.model.Magazine;
import by.shag.gritskevich.model.Newspaper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public Book bookV1() {
        Book bookV1 = new Book();
        bookV1.setVersion(1);
        return bookV1;
    }

    @Bean
    public Book bookV2() {
        Book bookV2 = new Book();
        bookV2.setVersion(2);
        return bookV2;
    }

    @Bean
    public Book bookV3() {
        Book bookV3 = new Book();
        bookV3.setVersion(3);
        return bookV3;
    }

    @Bean
    public Magazine magazineV1() {
        Magazine magazineV1 = new Magazine();
        magazineV1.setVersion(1);
        return magazineV1;
    }

    @Bean
    public Magazine magazineV2() {
        Magazine magazineV2 = new Magazine();
        magazineV2.setVersion(2);
        return magazineV2;
    }

    @Bean
    public Newspaper newspaperV1() {
        Newspaper newspaperV1 = new Newspaper();
        newspaperV1.setVersion(1);
        return newspaperV1;
    }
}
