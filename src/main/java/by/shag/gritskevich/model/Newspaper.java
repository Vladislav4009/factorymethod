package by.shag.gritskevich.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class Newspaper implements PrintedProduct {

    @Value("${printing.NewsPaper.name}")
    private String name;
    @Value("${printing.NewsPaper.mainPage}")
    private String mainPage;
    @Value("${printing.NewsPaper.pageSize}")
    private Integer pageSize;
    private Integer version;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void showMainPage() {
        System.out.println(mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Newspaper{" +
                "name='" + name + '\'' +
                ", mainPage='" + mainPage + '\'' +
                ", pageSize=" + pageSize +
                ", version=" + version +
                '}';
    }
}
