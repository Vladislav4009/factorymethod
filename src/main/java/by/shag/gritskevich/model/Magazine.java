package by.shag.gritskevich.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class Magazine implements PrintedProduct {

    @Value("${printing.Magazine.name}")
    private String name;
    @Value("${printing.Magazine.mainPage}")
    private String mainPage;
    @Value("${printing.Magazine.pageSize}")
    private Integer pageSize;
    private Integer version;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void showMainPage() {
        System.out.println(mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "name='" + name + '\'' +
                ", mainPage='" + mainPage + '\'' +
                ", pageSize=" + pageSize +
                ", version=" + version +
                '}';
    }
}
