package by.shag.gritskevich.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class Book implements PrintedProduct {

    @Value("${printing.book.name}")
    private String name;
    @Value("${printing.book.mainPage}")
    private String mainPage;
    @Value("${printing.book.pageSize}")
    private Integer pageSize;
    private Integer version;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void showMainPage() {
        System.out.println(mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", mainPage='" + mainPage + '\'' +
                ", pageSize=" + pageSize +
                ", version=" + version +
                '}';
    }
}
