package by.shag.gritskevich.model;

public interface PrintedProduct {

    String getName();

    void showMainPage();

    int getPageSize();
}
