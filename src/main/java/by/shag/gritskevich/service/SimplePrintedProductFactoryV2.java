package by.shag.gritskevich.service;

import by.shag.gritskevich.model.Book;
import by.shag.gritskevich.model.Magazine;
import by.shag.gritskevich.model.Newspaper;
import by.shag.gritskevich.model.PrintedProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimplePrintedProductFactoryV2 implements PrintedProductFactory {

    @Autowired
    private Book bookV2;
    @Autowired
    private Magazine magazineV1;
    @Autowired
    private Newspaper newspaperV1;

    @Override
    public PrintedProduct getProductByType(AllPrintedProduct allPrintedProduct) {
        return allPrintedProduct == AllPrintedProduct.BOOK ? bookV2 :
                allPrintedProduct == AllPrintedProduct.MAGAZINE ? magazineV1 : newspaperV1;
    }
}
