package by.shag.gritskevich.service;

import by.shag.gritskevich.model.PrintedProduct;

public interface PrintedProductFactory {

    PrintedProduct getProductByType(AllPrintedProduct allPrintedProduct);
}
