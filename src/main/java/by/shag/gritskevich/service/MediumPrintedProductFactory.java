package by.shag.gritskevich.service;

import by.shag.gritskevich.model.PrintedProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MediumPrintedProductFactory {

    @Autowired
    private SimplePrintedProductFactoryV1 simplePrintedProductFactoryV1;
    @Autowired
    private SimplePrintedProductFactoryV2 simplePrintedProductFactoryV2;

    public PrintedProduct getProduct(Integer version, AllPrintedProduct allPrintedProduct) {

        if (version == 1) {
            return simplePrintedProductFactoryV1.getProductByType(allPrintedProduct);
        } else {
            return simplePrintedProductFactoryV2.getProductByType(allPrintedProduct);
        }

    }
}
