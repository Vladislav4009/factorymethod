package by.shag.gritskevich.service;

public enum AllPrintedProduct {

    BOOK,
    MAGAZINE,
    NEWSPAPER
}
