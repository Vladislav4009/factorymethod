package by.shag.gritskevich.service;

import by.shag.gritskevich.model.Book;
import by.shag.gritskevich.model.Magazine;
import by.shag.gritskevich.model.Newspaper;
import by.shag.gritskevich.model.PrintedProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimplePrintedProductFactoryV1 implements PrintedProductFactory{

    @Autowired
    private Book bookV3;
    @Autowired
    private Magazine magazineV2;
    @Autowired
    private Newspaper newspaperV1;

    public void showAllProducts() {
        System.out.println(bookV3);
        System.out.println(magazineV2);
        System.out.println(newspaperV1);
    }

    public PrintedProduct getProductByType(AllPrintedProduct allPrintedProduct) {
        return allPrintedProduct == AllPrintedProduct.BOOK ? bookV3 :
                allPrintedProduct == AllPrintedProduct.MAGAZINE ? magazineV2 : newspaperV1;
    }
}
