package by.shag.gritskevich;

import by.shag.gritskevich.model.PrintedProduct;
import by.shag.gritskevich.service.AllPrintedProduct;
import by.shag.gritskevich.service.MediumPrintedProductFactory;
import by.shag.gritskevich.service.PrintedProductFactory;
import by.shag.gritskevich.service.SimplePrintedProductFactoryV1;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);

        Map<String, PrintedProduct> beansOfType = context.getBeansOfType(PrintedProduct.class);
//        System.out.println(beansOfType.toString());
//        Stream.of(beansOfType)
//                .collect(Collectors.toList())
//                .forEach(System.out::println);
//        context.getBean(PrintedProductService.class).showAllProducts();

//        SimplePrintedProductFactoryV1 service = context.getBean(SimplePrintedProductFactoryV1.class);
//        System.out.println(
//                service.getProductByType(AllPrintedProduct.BOOK));
//        System.out.println(
//                service.getProductByType(AllPrintedProduct.MAGAZINE));
//        System.out.println(
//                service.getProductByType(AllPrintedProduct.NEWSPAPER));

        MediumPrintedProductFactory factory = context.getBean(MediumPrintedProductFactory.class);
        System.out.println(factory.getProduct(1, AllPrintedProduct.BOOK));

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
